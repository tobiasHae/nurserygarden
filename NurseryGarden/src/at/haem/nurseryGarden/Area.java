package at.haem.nurseryGarden;

import java.util.ArrayList;
import java.util.List;

public class Area {

	private String name;
	private int size;
	private List<Tree> trees;
	
	public Area(String name, int size) {
		super();
		this.name = name;
		this.size = size;
		this.trees = new ArrayList<Tree>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}
	
	public void addTree(Tree tree) {
		this.trees.add(tree);
	}
	
	
	public void fertilizeAll(){
		for(Tree tree : trees) {
		
		}
	}
	
	
}
