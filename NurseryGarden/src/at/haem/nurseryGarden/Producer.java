package at.haem.nurseryGarden;

public class Producer {

	private String name;
	private Area area;
	
	public Producer(String name, Area area) {
		super();
		this.name = name;
		this.area = area;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Area getArea() {
		return area;
	}

	public void setArea(Area area) {
		this.area = area;
	}
	
	
	
	
}
