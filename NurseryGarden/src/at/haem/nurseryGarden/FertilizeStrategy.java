package at.haem.nurseryGarden;

public interface FertilizeStrategy {
	
	public void doFertilize();
}
