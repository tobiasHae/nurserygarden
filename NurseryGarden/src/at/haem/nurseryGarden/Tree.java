package at.haem.nurseryGarden;

public class Tree implements FertilizeStrategy{

	private int maxHeight;
	private int maxTrunkDiameter;
	
	public Tree(int maxHeight, int maxTrunkDiameter) {
		super();
		this.maxHeight = maxHeight;
		this.maxTrunkDiameter = maxTrunkDiameter;
	}

	public int getMaxHeight() {
		return maxHeight;
	}

	public void setMaxHeight(int maxHeight) {
		this.maxHeight = maxHeight;
	}

	public int getMaxTrunkDiameter() {
		return maxTrunkDiameter;
	}

	public void setMaxTrunkDiameter(int maxTrunkDiameter) {
		this.maxTrunkDiameter = maxTrunkDiameter;
	}

	
	@Override
	public void doFertilize() {
		System.out.println("This is a test");
		
	}
	
	
	
	
	
}
